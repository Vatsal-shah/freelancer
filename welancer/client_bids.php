<?php session_start();
include 'client_header.php';	
?>
<?php 

if(empty($_SESSION['user'])){
	echo "<script>location.href='login.php';</script>";
}

?>

<!-- Dashboard Container -->
<div class="dashboard-container">

	<?php include 'client_sidebar.php'; ?>


	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>My Active Bids</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Dashboard</a></li>
						<li>My Active Bids</li>
					</ul>
				</nav>
			</div>
			<?php 

				$b = "SELECT *, B.id as B_id, CONCAT(D.first_name,' ', D.last_name) as name FROM bid B LEFT JOIN post P ON P.id=B.post_id LEFT JOIN developer D On D.id=B.developer_id where B.client_id=".$_SESSION['user']['id'];
				$mt = mysqli_query($conn , $b);

			?>

			<!-- Row -->
			<div class="row">
					
				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">
						<div class="headline">
							<h3><i class="icon-material-outline-gavel"></i> Bids List</h3>
						</div>
						
						<!-- Headline -->
						<?php while($r = mysqli_fetch_array($mt)){  ?>
						<div class="headline">
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								<li>
									<!-- Job Listing -->
									<div class="job-listing width-adjustment">

										<!-- Job Listing Details -->
										<div class="job-listing-details">

											<!-- Details -->
											<div class="job-listing-description">
												<h3 class="job-listing-title"><a href="#"><?php echo $r['name'].' - '.$r['job_title']; ?></a></h3>
											</div>
										</div>
									</div>
									
									<!-- Task Details -->
									<ul class="dashboard-task-info">
										<li><strong><?php echo $r['budget']; ?></strong><span>Rate</span></li>
										<li><strong><?php echo $r['date']; ?></strong><span>Date</span></li>
									</ul>

									<!-- Buttons -->
									<div class="buttons-to-right always-visible">
										<!-- <a href="#small-dialog" class="popup-with-zoom-anim button dark ripple-effect ico" title="Edit Bid" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>
										<a href="#" class="button red ripple-effect ico" title="Cancel Bid" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a> -->
										
										<!-- <div class="col-xl-2"> 
											<a href="#" class="margin-top-15 button full-width button-sliding-icon ripple-effect " name="accept">Accept<i class="icon-material-outline-arrow-right-alt"></i></a>
										</div> -->
									
										
									</div>

								</li>
							</ul>	
						</div>
						<?php } ?>

					</div>
				</div>

			</div>
			
			<!-- Row / End -->

			<!-- Edit Bid Popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
			<li><a href="#tab">Edit Bid</a></li>
		</ul>

		<div class="popup-tabs-container">

			<!-- Tab -->
			<div class="popup-tab-content" id="tab">
						
					<!-- Bidding -->
					<div class="bidding-widget">
						<!-- Headline -->
						<span class="bidding-detail">Set your <strong>minimal hourly rate</strong></span>

						<!-- Price Slider -->
						<div class="bidding-value">$<span id="biddingVal"></span></div>
						<input class="bidding-slider" type="text" value="" data-slider-handle="custom" data-slider-currency="$" data-slider-min="10" data-slider-max="60" data-slider-value="40" data-slider-step="1" data-slider-tooltip="hide" />
						
						<!-- Headline -->
						<span class="bidding-detail margin-top-30">Set your <strong>delivery time</strong></span>

						<!-- Fields -->
						<div class="bidding-fields">
							<div class="bidding-field">
								<!-- Quantity Buttons -->
								<div class="qtyButtons with-border">
									<div class="qtyDec"></div>
									<input type="text" name="qtyInput" value="2">
									<div class="qtyInc"></div>
								</div>
							</div>
							<div class="bidding-field">
								<select class="selectpicker default with-border">
									<option selected>Days</option>
									<option>Hours</option>
								</select>
							</div>
						</div>
				</div>
				
				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect" type="submit">Save Changes <i class="icon-material-outline-arrow-right-alt"></i></button>

			</div>

		</div>
	</div>
</div>
<!-- Edit Bid Popup / End -->


<?php include 'mini-footer.php'; ?>