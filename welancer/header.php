<?php include 'db.php'; ?>
<!doctype html>
<html lang="en">
<head>

<!-- Basic Page Needs
================================================== -->
<title>Hireo</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/blue.css">

</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->
<header id="header-container" class="fullwidth transparent">

	<!-- Header -->
	<div id="header">
		<div class="container">
			
			<!-- Left Side Content -->
			<div class="left-side">
				
				<!-- Logo -->
				<div id="logo">
					<a href="home.php"><img src="images/logo.jpg" alt=""></a>
				</div>

				<!-- Main Navigation -->
				<nav id="navigation">
					<ul id="responsive">

						<li><a href="home.php" class="current">Home</a>
							<!-- <ul class="dropdown-nav">
								<li><a href="index.html">Home 1</a></li>
								<li><a href="index-2.html">Home 2</a></li>
								<li><a href="index-3.html">Home 3</a></li>
							</ul> -->
						</li>

						<li><a href="browse-job.php">Browse Jobs</a>
							<!-- <ul class="dropdown-nav">
								<li>
									<ul class="dropdown-nav">
										<li><a href="jobs-list-layout-full-page-map.html">Full Page List + Map</a></li>
										<li><a href="jobs-grid-layout-full-page-map.html">Full Page Grid + Map</a></li>
										<li><a href="jobs-grid-layout-full-page.html">Full Page Grid</a></li>
										<li><a href="jobs-list-layout-1.html">List Layout 1</a></li>
										<li><a href="jobs-list-layout-2.html">List Layout 2</a></li>
										<li><a href="jobs-grid-layout.html">Grid Layout</a></li>
									</ul>
								</li>
								<li><a href="browse-task.php">Browse Tasks</a>
									<ul class="dropdown-nav">
										<li><a href="tasks-list-layout-1.html">List Layout 1</a></li>
										<li><a href="tasks-list-layout-2.html">List Layout 2</a></li>
										<li><a href="tasks-grid-layout.html">Grid Layout</a></li>
										<li><a href="tasks-grid-layout-full-page.html">Full Page Grid</a></li>
									</ul>
								</li>
								<li><a href="browse-companies.php">Browse Companies</a></li>
								<li><a href="job-page.php">Job Page</a></li>
								<li><a href="task-page.php">Task Page</a></li>
								<li><a href="company-profile.php">Company Profile</a></li>
							</ul> -->
						</li>

						<li><a href="#">For Employers</a>
							<ul class="dropdown-nav">
								<li><a href="find-freelancer.php">Find a Freelancer</a>
									<!-- <ul class="dropdown-nav">
										<li><a href="freelancers-grid-layout-full-page.html">Full Page Grid</a></li>
										<li><a href="freelancers-grid-layout.html">Grid Layout</a></li>
										<li><a href="freelancers-list-layout-1.html">List Layout 1</a></li>
										<li><a href="freelancers-list-layout-2.html">List Layout 2</a></li>
									</ul> -->
								</li>
								<!-- <li><a href="freelancer-profile.php">Freelancer Profile</a></li> -->
								<li><a href="job-post.php">Post a Job</a></li>
								<!-- <li><a href="task-post.php">Post a Task</a></li> -->
							</ul>
						</li>

						<li><a href="#">Dashboard</a>
							<ul class="dropdown-nav">
								<li><a href="dashboard.php">Dashboard</a></li>
								<!-- <li><a href="messages.php">Messages</a></li> -->
								<li><a href="bookmarks.php">Bookmarks</a></li>
								<li><a href="reviews.php">Reviews</a></li>
								<li><a href="manage-jobs.php">Jobs</a>
									<ul class="dropdown-nav">
										<li><a href="manage-jobs.php">Manage Jobs</a></li>
										<li><a href="manage-candidates.php">Manage Candidates</a></li>
										<li><a href="job-post.php">Post a Job</a></li>
									</ul>
								</li>
								<li><a href="dashboard-manage-tasks.html">Manage</a>
									<ul class="dropdown-nav">
										<li><a href="manage-tasks.php">Manage Tasks</a></li>
										<li><a href="manage-bidders.php">Manage Bidders</a></li>
										<li><a href="active-bids.php">My Active Bids</a></li>
										<!-- <li><a href="task-post.php">Post a Task</a></li> -->
									</ul>
								</li>
								<li><a href="settings.php">Settings</a></li>
							</ul>
						</li>

						<li><a href="#">Pages</a>
							<ul class="dropdown-nav">
								<!-- <li><a href="pages-blog.html">Blog</a></li> -->
								<li><a href="plans.php">Pricing Plans</a></li>
								<li><a href="checkout-page.php">Checkout Page</a></li>
								<!-- <li><a href="pages-invoice-template.html">Invoice Template</a></li>
								<li><a href="pages-user-interface-elements.html">User Interface Elements</a></li>
								<li><a href="pages-icons-cheatsheet.html">Icons Cheatsheet</a></li>
								<li><a href="pages-login.html">Login & Register</a></li>
								<li><a href="pages-404.html">404 Page</a></li> -->
								<li><a href="contact.php">Contact</a></li>
							</ul>
						</li>

					</ul>
				</nav>
				<div class="clearfix"></div>
				<!-- Main Navigation / End -->
				
			</div>
			<!-- Left Side Content / End -->


			<!-- Right Side Content / End -->
			<div class="right-side">

				<div class="header-widget">
					<a href="login.php" class="log-in-button"><i class="icon-feather-log-in"></i> <span>Log In</span></a>
				</div>

				<!-- Mobile Navigation Button -->
				<span class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</span>

			</div>
			<!-- Right Side Content / End -->

		</div>
	</div>
	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->