<!-- Dashboard Sidebar
	================================================== -->
	<div class="dashboard-sidebar">
		<div class="dashboard-sidebar-inner" data-simplebar>
			<div class="dashboard-nav-container">

				<!-- Responsive Navigation Trigger -->
				<a href="#" class="dashboard-responsive-nav-trigger">
					<span class="hamburger hamburger--collapse" >
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</span>
					<span class="trigger-title">Dashboard Navigation</span>
				</a>
				
				<!-- Navigation -->
				<div class="dashboard-nav">
					<div class="dashboard-nav-inner">

						<ul data-submenu-title="Start">
							<li><a href="dev_dashboard.php"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
<!-- 							<li><a href="messages.php"><i class="icon-material-outline-question-answer"></i> Messages <span class="nav-tag">2</span></a></li> -->
							<li><a href="dev_bookmark.php"><i class="icon-material-outline-star-border"></i> Bookmarks</a></li>
							<li><a href="dev_reviews.php"><i class="icon-material-outline-rate-review"></i> Reviews</a></li>
						</ul>
						
						<ul data-submenu-title="Organize and Manage">
							<li><a href="#"><i class="icon-material-outline-business-center"></i> Jobs</a>
								<ul>
									<li><a href="active_bids.php">My Active Bids</a></li>
									<li><a href="active_jobs.php">My Active Jobs</a></li>
									<!-- <li><a href="job-post.php">Post a Job</a></li> -->
								</ul>	
							</li>
							<!-- <li><a href="#"><i class="icon-material-outline-assignment"></i> Tasks</a>
								<ul>
									<li><a href="manage-tasks.php">Manage Tasks <span class="nav-tag">2</span></a></li>
									<li><a href="manage-bidders.php">Manage Bidders</a></li>
									<li><a href="active-bids.php">My Active Bids <span class="nav-tag">4</span></a></li>
									<li><a href="task-post.php">Post a Task</a></li>
								</ul>	
							</li> -->
						</ul>

						<ul data-submenu-title="Account">
							<li><a href="dev_setting.php"><i class="icon-material-outline-settings"></i> Settings</a></li>
							<li><a href="logout.php"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
						</ul>
						
					</div>
				</div>
				<!-- Navigation / End -->

			</div>
		</div>
	</div>
	<!-- Dashboard Sidebar / End -->
