<?php include 'header-profile.php' ;


if ($_POST['submit']) {
	 	$pname = $_POST['p_title'];
	 	$cat =$_POST['category'];
	 	$location =$_POST['location'];
	 	$minimum = $_POST['min'];
	 	$maximum = $_POST['max'];
	 	$skills = $_POST['skills'];
	 	$desc =$_POST['p_describe'];
	 	
	 


		$sql = "INSERT INTO task (pro_name, category, location, min, maximum, skills, p_describe) VALUES ('$pname', '$cat', '$location', '$minimum', '$maximum', '$skills', '$desc')";

		if ($conn->query($sql) === TRUE) { 
		    echo "New record created successfully";
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}    
	}
?>

<!-- Dashboard Container -->
<div class="dashboard-container">

	<?php include 'sidebar.php'; ?>

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Post a Task</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Dashboard</a></li>
						<li>Post a Task</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-feather-folder-plus"></i> Task Submission Form</h3>
						</div>

						<div class="content with-padding padding-bottom-10">
							<form action="" method="Post">
							<div class="row">


								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Project Name</h5>
										<input type="text" class="with-border" name="p_title" placeholder="e.g. build me a website">
									</div>
								</div>
								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Category</h5>
										<select name="category">
											<option>Admin Support</option>
											<option>Customer Service</option>
											<option>Data Analytics</option>
											<option>Design & Creative</option>
											<option>Legal</option>
											<option>Software Developing</option>
											<option>IT & Networking</option>
											<option>Writing</option>
											<option>Translation</option>
											<option>Sales & Marketing</option>
										</select>
									</div>
								</div>
								<div class="col-xl-4">
									<div class="submit-field">
										<h5>Location  <i class="help-icon" data-tippy-placement="right" title="Leave blank if it's an online job"></i></h5>
										<div class="input-with-icon">
											<div id="autocomplete-container">
												<input id="autocomplete-input"  name="location" class="with-border" type="text" placeholder="Anywhere">
											</div>
											<i class="icon-material-outline-location-on"></i>
										</div>
									</div>
								</div>


								
									<div class="col-xl-6">
									<div class="submit-field">
										<h5>What is your estimated budget?</h5>
										<div class="row">
											<div class="col-xl-6">
												<div class="input-with-icon">
													<input class="with-border" type="text" name="min" placeholder="Minimum">
													<i class="currency">USD</i>
												</div>
											</div>
											<div class="col-xl-6">
												<div class="input-with-icon">
													<input class="with-border" type="text" placeholder="Maximum" name="max">
													<i class="currency">USD</i>
												</div>
											</div>
										</div>
									
									</div>
								</div>
								<div class="col-xl-6">
									<div class="submit-field">
										<h5>What skills are required? <i class="help-icon" data-tippy-placement="right" title="Up to 5 skills that best describe your project"></i></h5>
										<div class="keywords-container">
											<div class="keyword-input-container">
												<input type="text" class="keyword-input with-border" placeholder="Add Skills"/>
												<button class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
											</div>
											<div class="keywords-list"><!-- keywords go here --></div>
											<div class="clearfix"></div>
										</div>

									</div>
								</div>

								<div class="col-xl-12">
									<div class="submit-field">
										<h5>Describe Your Project</h5>
										<textarea cols="30" rows="5" class="with-border" name="p_describe"></textarea>
										<div class="uploadButton margin-top-30">
											<input class="uploadButton-input" type="file" accept="image/*, application/pdf" id="upload" multiple/>
											<label class="uploadButton-button ripple-effect" for="upload">Upload Files</label>
											<span class="uploadButton-file-name">Images or documents that might be helpful in describing your project</span>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-12">
					<input type="submit" name="submit"  class="button ripple-effect big margin-top-30" name="submit" value="Post a Task">
				</div>

			</div>
			<!-- Row / End -->
			</form>
			

<?php include 'mini-footer.php' ?>