<?php session_start(); ?>
<?php include 'db.php'; ?>

<?php include 'header-profile.php'; ?>


<!-- Dashboard Container -->
<div class="dashboard-container">

	<?php include 'sidebar.php'; ?>


	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>My Active Bids</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Dashboard</a></li>
						<li>My Active Bids</li>
					</ul>
				</nav>
			</div>
			<?php 
				 // $b = $_SESSION['user']['id'] ;
				 // echo $_SESSION['user']['id'];

				// $b="SELECT post.job_title, bid.budget , bid.date FROM post right JOIN bid ON post.client_id=bid.client_id where bid.client_id = ".$_SESSION['user']['id'];

				$b = "SELECT *, CONCAT(D.first_name,' ', D.last_name) as name FROM bid B LEFT JOIN post P ON P.id=B.post_id LEFT JOIN developer D On D.id=B.developer_id where b.client_id=".$_SESSION['user']['id'];
				$mt = mysqli_query($conn , $b);

			?>

			<!-- Row -->
			<div class="row">
					
				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">
						<?php while($r = mysqli_fetch_array($mt)){  ?>
						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-gavel"></i> Bids List</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								<li>
									<!-- Job Listing -->
									<div class="job-listing width-adjustment">

										<!-- Job Listing Details -->
										<div class="job-listing-details">

											<!-- Details -->
											<div class="job-listing-description">
												<h3 class="job-listing-title"><a href="#"><?php echo $r['name'].' - '.$r['job_title']; ?></a></h3>
											</div>
										</div>
									</div>
									
									<!-- Task Details -->
									<ul class="dashboard-task-info">
										<li><strong><?php echo $r['budget']; ?></strong><span>Rate</span></li>
										<li><strong><?php echo $r['date']; ?></strong><span>Date</span></li>
									</ul>

									<!-- Buttons -->
									<div class="buttons-to-right always-visible">
										<a href="#small-dialog" class="popup-with-zoom-anim button dark ripple-effect ico" title="Edit Bid" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>
										<a href="#" class="button red ripple-effect ico" title="Cancel Bid" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
										
										<div class="col-xl-2"> 
											<a href="" class="margin-top-15 button full-width button-sliding-icon ripple-effect "><?php mysqli_query($conn , "UPDATE bid SET duration ='1' where id= '54'");?>Accept<i class="icon-material-outline-arrow-right-alt"></i></a>
										</div>
									
										
									</div>

								</li>
							</ul>	
						</div>
						<?php } ?>

					</div>
				</div>

			</div>
			
			<!-- Row / End -->



<?php include 'mini-footer.php'; ?>