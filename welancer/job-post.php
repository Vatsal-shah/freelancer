<?php session_start(); ?>

<?php include 'client_header.php'; ?>

<?php
if(empty($_SESSION['user'])){
	echo "<script>location.href='login.php';</script>";
}

	if (isset($_POST['submit'])) {
	    $title = $_POST['jtitle'];
	    $jtime = $_POST['time'];
	    $category = $_POST['j_category'];
	    $location =$_POST['location'];
	    $budget =$_POST['budget'];
	    $tags = $_POST['tags'];
	    $description =$_POST['desc'];



		$sql = "INSERT INTO post (job_title, j_time, j_category, location, budget, j_tags, j_description ) VALUES ( '$title',  '$jtime', '$category', '$location', '$budget',  '$tags', '$description')";

		if ($conn->query($sql) === TRUE) {
		    echo "New record created successfully";
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}  
	}  


?>

<!-- Dashboard Container -->
<div class="dashboard-container">

	<?php include 'client_sidebar.php'; ?>

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Post a Job</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Dashboard</a></li>
						<li>Post a Job</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">
				<form action="#" method="post">

					<!-- Dashboard Box -->
					<div class="col-xl-12">
						<div class="dashboard-box margin-top-0">

							<!-- Headline -->
							<div class="headline">
								<h3><i class="icon-feather-folder-plus"></i> Job Submission Form</h3>
							</div>

							<div class="content with-padding padding-bottom-10">
								<div class="row">

									<div class="col-xl-4">
										<div class="submit-field">
											<h5>Job Title</h5>
											<input type="text" name="jtitle" class="with-border">
										</div>
									</div>
									<div class="col-xl-4">
										<div class="submit-field">
											<h5>Job Type</h5>
											<select  name="time">
												<option>Full Time</option>
												<option>Freelance</option>
												<option>Part Time</option>
												<option>Internship</option>
												<option>Temporary</option>
											</select>
										</div>
									</div>

									<div class="col-xl-4">
										<div class="submit-field">
											<h5>Job Category</h5>
											<select  name="j_category">
												<option>Accounting and Finance</option>
												<option>Clerical & Data Entry</option>
												<option>Counseling</option>
												<option>Court Administration</option>
												<option>Human Resources</option>
												<option>Investigative</option>
												<option>IT and Computers</option>
												<option>Law Enforcement</option>
												<option>Management</option>
												<option>Miscellaneous</option>
												<option>Public Relations</option>
											</select>
										</div>
									</div>

									<div class="col-xl-4">
										<div class="submit-field">
											<h5>Location</h5>
											<div class="input-with-icon">
												<div id="autocomplete-container">
													<input id="autocomplete-input" name="location" class="with-border" type="text" placeholder="Type Address">
												</div>
												<i class="icon-material-outline-location-on"></i>
											</div>
										</div>
									</div>

									<div class="col-xl-4">
										<div class="submit-field">
											<h5>Budget</h5>
											<input type="text" class="with-border" name="budget">
										</div>
									</div>
									
									<!-- <div class="col-xl-4">
									<div class="submit-field">
										<h5>Tags <span>(optional)</span>  <i class="help-icon" data-tippy-placement="right" title="Maximum of 10 tags"></i></h5>
										<div class="keywords-container">
											<div class="keyword-input-container">
												<input type="text" class="keyword-input with-border" placeholder="e.g. job title, responsibilites"/>
												<button class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
											</div>
											<div class="keywords-list"></div>
											<div class="clearfix"></div>
										</div>

									</div>
									</div> -->


									<div class="col-xl-12">
										<div class="submit-field">
											<h5>Job Description</h5>
											<textarea cols="30" rows="5" name="desc" class="with-border"></textarea>
											<div class="uploadButton margin-top-30">
												<input class="uploadButton-input" type="file"  accept="image/*, application/pdf" id="upload" multiple/>
												<label class="uploadButton-button ripple-effect" for="upload">Upload Files</label>
												<span class="uploadButton-file-name">Images or documents that might be helpful in describing your job</span>
											</div>
										</div>
									</div>

									



								</div>
							</div>
						</div>
						<div class="col-xl-12">
							<input type="submit" name="submit"  class="button ripple-effect big margin-top-30" name="submit" value="Post a Job">
				</div>
				</div>

				
			</form>

			</div>
			<!-- Row / End -->

<?php include 'mini-footer.php' ?>
