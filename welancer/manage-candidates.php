<?php session_start(); ?>
<?php 

if(empty($_SESSION['user'])){
	echo "<script>location.href='login.php';</script>";
}

?>
<?php include 'client_header.php';?>

<!-- Dashboard Container -->
<div class="dashboard-container">

	<?php include 'client_sidebar.php'; ?>

	<?php
		// For Geting Job Title
		$query = "select * from post where id=".$_GET['id'];
		$res = mysqli_query($conn , $query);
		$result = mysqli_fetch_assoc($res);
		$job_title = $result['job_title'];
	?>

	<?php 
	// Hire Developer
	if(isset($_POST['B_id'])){
		
		$accept_query = "update bid set accept=1 where id=".$_POST['B_id'];

		if (mysqli_query($conn, $accept_query)) {
			$rjt_query = "update bid set accept=0 where id<>".$_POST['B_id']." and post_id=".$_GET['id'];
			mysqli_query($conn, $rjt_query);
		    $status = "Developer Hired Successfully";
		} else {
		    $status = "Error..";
		}

	}

	//Completed By Client
	if(isset($_POST['CB_id'])){

		$complete_query = "update bid set completed_by_client=1, completed_by_developer=1 where id=".$_POST['CB_id'];

		if (mysqli_query($conn, $complete_query)) {
		    $status = "Job Completed Successfully";
		} else {
		    $status = "Error..";
		}
	
	}

	?>

	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Manage Candidates</h3>
				<span class="margin-top-7">Job Applications for <a href="#"><?php echo $job_title; ?></a></span>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Dashboard</a></li>
						<li>Manage Candidates</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">
				<?php 
				if(isset($status)){
				?>
				<div class="col-xl-12">
					<div class="notification success closeable">
					<?php echo $status; ?>
						<a class="close"></a>
					</div>
				</div>	
				<?php	
				}
				?>	
				
				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<?php
						//For Current Job Candidates
						$can_query  = "SELECT *, B.id as B_id, D.id as D_id, CONCAT(D.first_name,' ', D.last_name) as name from bid B left join developer D On D.id=B.developer_id where B.post_id=".$_GET['id'];
						$res = mysqli_query($conn , $can_query);
						$num = mysqli_num_rows($res);

						?>

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-supervisor-account"></i> <?php echo $num; ?> Candidates</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								<?php

								while($result = mysqli_fetch_array($res)){
								?>

								<li>
									<!-- Overview -->
									<div class="freelancer-overview manage-candidates">
										<div class="freelancer-overview-inner">

											<!-- Avatar -->
											<div class="freelancer-avatar">
												<div class="verified-badge"></div>
												<a href="#"><img src="images/user-avatar-big-03.jpg" alt=""></a>
											</div>

											<!-- Name -->
											<div class="freelancer-name">
												<h4><a href="#"><?php echo $result['name']; ?> </a></h4>

												<!-- Details -->
												<span class="freelancer-detail-item"><a href="#"><i class="icon-feather-mail"></i><?php echo $result['email']; ?></a></span>
												
												<ul class="dashboard-task-info">
										<li><strong><?php echo $result['budget']; ?></strong><span>Rate</span></li>
										<li><strong><?php echo $result['date']; ?></strong><span>Date</span></li>
									</ul>

												<!-- Rating -->
												<div class="freelancer-rating">
													<div class="star-rating" data-rating="5.0"></div>
												</div>

												<!-- Buttons -->
												<div class="buttons-to-right always-visible margin-top-25 margin-bottom-5">
													<?php 
													
													if( $result['accept'] == '1' ){ ?>
														
														
														<?php

														if( $result['completed_by_client'] == '1'){ ?>
															
															<span class="dashboard-status-button green">Job Completed.</span>

															<?php
														}else{ ?>

														<span class="dashboard-status-button green">Hired</span>
														<form action="" method="post">
															<input type="hidden" name="CB_id" value="<?php echo $result['B_id']; ?>">
															<button type="submit" class="button ripple-effect">Complete Job</button>
														</form>

														<?php
														}

														?>

													<?php

													}elseif( $result['accept'] == '0' ){

													}else{ ?>

													<form action="" method="post">
														<input type="hidden" name="B_id" value="<?php echo $result['B_id']; ?>">
														<button type="submit" class="button ripple-effect">Accept</button>
													</form>
													
													<a href="<?php echo $result['B_id']; ?>" class="button gray ripple-effect ico" title="Remove Candidate" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
													<?php }

													?>
												</div>
											</div>
										</div>
									</div>
								</li>

								<?php } ?>

							</ul>
						</div>
					</div>
				</div>

			</div>
			<!-- Row / End -->
			
<!-- Send Direct Message Popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
			<li><a href="#tab">Send Message</a></li>
		</ul>

		<div class="popup-tabs-container">

			<!-- Tab -->
			<div class="popup-tab-content" id="tab">
				
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>Direct Message To Sindy</h3>
				</div>
					
				<!-- Form -->
				<form method="post" id="send-pm">
					<textarea name="textarea" cols="10" placeholder="Message" class="with-border" required></textarea>
				</form>
				
				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect" type="submit" form="send-pm">Send <i class="icon-material-outline-arrow-right-alt"></i></button>

			</div>

		</div>
	</div>
</div>
<!-- Send Direct Message Popup / End -->


<?php include 'mini-footer.php'; ?>