<?php session_start(); ?>
<?php 

if(empty($_SESSION['user'])){
	echo "<script>location.href='login.php';</script>";
}

?>




<?php include 'client_header.php';?>

<!-- Dashboard Container -->
<div class="dashboard-container">

	<?php include 'client_sidebar.php'; ?>


	<?php
	

	if(isset($_POST['id'])){

		//echo $_POST['id'];
		
		$delete = "delete from post where id=".$_POST['id'];

		if (mysqli_query($conn, $delete)) {
			
		    $status = "job removed Successfully";
		} else {
		    $status = "Error while removing job";
		}

	 }

	?>


	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Manage Jobs</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Dashboard</a></li>
						<li>Manage Jobs</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
			<div class="row">
				<?php 
				if(isset($status)){
				?>
				<div class="col-xl-12">
					<div class="notification success closeable">
					<?php echo $status; ?>
						<a class="close"></a>
					</div>
				</div>	
				<?php	
				}
				?>

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-business-center"></i> My Job Listings</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">


								<?php
									$query = "select * from post where client_id=".$_SESSION['user']['id'];
									$res = mysqli_query($conn , $query);
								?>

								<?php 
								while($result = mysqli_fetch_array($res)){
								?>	

								<li>
									<!-- Job Listing -->
									<div class="job-listing">

										<!-- Job Listing Details -->
										<div class="job-listing-details">

											<!-- Logo -->
<!-- 											<a href="#" class="job-listing-company-logo">
												<img src="images/company-logo-05.png" alt="">
											</a> -->

											<!-- Details -->
											<div class="job-listing-description">
												<h3 class="job-listing-title"><a href="manage-candidates.php?id=<?php echo $result['id']; ?>"><?php echo $result['job_title']; ?></a> <span class="dashboard-status-button green">Pending Approval</span></h3>

												<!-- Job Listing Footer -->
												<div class="job-listing-footer">
													<ul>
														<li><i class="icon-material-outline-date-range"></i> Posted on 10 July, 2018</li>
														<li><i class="icon-material-outline-date-range"></i> Expiring on 10 August, 2018</li>
													</ul>
												</div>
											</div>
										</div>
									</div>

									<!-- Buttons -->
									<div class="buttons-to-right always-visible">
										<!-- <a href="dashboard-manage-candidates.html" class="button ripple-effect"><i class="icon-material-outline-supervisor-account"></i> Manage Candidates <span class="button-info">0</span></a> -->
									
										<a href="#" class="button gray ripple-effect ico" title="Edit" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>

									<form action="" method="post">
										<input type="hidden" name="id" value="<?php echo $result['id']; ?>">
										<button type="submit" class="button gray ripple-effect ico" title="Remove" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></button>
									</form>
									</div>
								</li>

								<?php } ?>

							</ul>
						</div>
					</div>
				</div>

			</div>
			<!-- Row / End -->

<?php include 'mini-footer.php'; ?>