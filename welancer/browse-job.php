<?php session_start(); ?>
<?php 

if(empty($_SESSION['user'])){
	echo "<script>location.href='login.php';</script>";
}

?>
<?php include 'dev_header.php';?>

<?php 
$p = "select * FROM post";
$c = mysqli_query($conn , $p);

?>

<!-- Spacer -->
<div class="margin-top-90"></div>
<!-- Spacer / End-->

<!-- Page Content
================================================== -->
<div class="container">
	<div class="row">
		<div class="col-xl-3 col-lg-4">
			<div class="sidebar-container">
				
				<!-- Location -->
				<div class="sidebar-widget">
					<h3>Location</h3>
					<div class="input-with-icon">
						<div id="autocomplete-container">
							<input id="autocomplete-input" type="text" placeholder="Location">
						</div>
						<i class="icon-material-outline-location-on"></i>
					</div>
				</div>

				
				<!-- Keywords -->
				<div class="sidebar-widget">
					<h3>Keywords</h3>
					<div class="keywords-container">
						<div class="keyword-input-container">
							<input type="text" class="keyword-input" placeholder="e.g. job title"/>
							<button class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
						</div>
						<div class="keywords-list"><!-- keywords go here --></div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				<!-- Category -->
				<?php 
				if (isset($_POST['j_category'])) {

					$category = $_POST['j_category'];
					switch ($category) {
        				case 'acc':
            			$q = "select * from post where j_category=".$category;
            			break;

            			case 'data':
            			$q = "select * from post where j_category=".$category;
            			break;

            			case 'it':
            			$q = "select * from post where j_category=".$category;
            			break;
        			}


					
					
				}


				?>
				
										<div class="submit-field">
											<h5>Job Category</h5>
											<select  name="j_category">
												<option value="acc">Accounting and Finance</option>
												<option value="data">Clerical & Data Entry</option>
												<option value="conc">Counseling</option>
												<option value="ca">Court Administration</option>
												<option value="hr">Human Resources</option>
												<option value="inve">Investigative</option>
												<option value="it">IT and Computers</option>
												<option value="le">Law Enforcement</option>
												<option value="mng">Management</option>
												<option value="mis">Miscellaneous</option>
												<option value="pr">Public Relations</option>
											</select>
										</div>				
				<!-- Job Types -->
				<div class="sidebar-widget">
					<h3>Job Type</h3>

					<div class="switches-list">
						<div class="switch-container">
							<label class="switch"><input type="checkbox"><span class="switch-button"></span> Freelance</label>
						</div>

						<div class="switch-container">
							<label class="switch"><input type="checkbox"><span class="switch-button"></span> Full Time</label>
						</div>

						<div class="switch-container">
							<label class="switch"><input type="checkbox"><span class="switch-button"></span> Part Time</label>
						</div>

						<div class="switch-container">
							<label class="switch"><input type="checkbox"><span class="switch-button"></span> Internship</label>
						</div>
						<div class="switch-container">
							<label class="switch"><input type="checkbox"><span class="switch-button"></span> Temporary</label>
						</div>
					</div>

				</div>

				<!-- Salary -->
				<div class="sidebar-widget">
					<h3>Salary</h3>
					<div class="margin-top-55"></div>

					<!-- Range Slider -->
					<input class="range-slider" type="text" value="" data-slider-currency="$" data-slider-min="1500" data-slider-max="15000" data-slider-step="100" data-slider-value="[1500,15000]"/>
				</div>

				<!-- Tags -->
				<div class="sidebar-widget">
					<h3>Tags</h3>

					<div class="tags-container">
						<div class="tag">
							<input type="checkbox" id="tag1"/>
							<label for="tag1">front-end dev</label>
						</div>
						<div class="tag">
							<input type="checkbox" id="tag2"/>
							<label for="tag2">angular</label>
						</div>
						<div class="tag">
							<input type="checkbox" id="tag3"/>
							<label for="tag3">react</label>
						</div>
						<div class="tag">
							<input type="checkbox" id="tag4"/>
							<label for="tag4">vue js</label>
						</div>
						<div class="tag">
							<input type="checkbox" id="tag5"/>
							<label for="tag5">web apps</label>
						</div>
						<div class="tag">
							<input type="checkbox" id="tag6"/>
							<label for="tag6">design</label>
						</div>
						<div class="tag">
							<input type="checkbox" id="tag7"/>
							<label for="tag7">wordpress</label>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

			</div>
		</div>
		<div class="col-xl-9 col-lg-8 content-left-offset">

			<h3 class="page-title"> Browse Posts</h3>
<!-- 
			<div class="notify-box margin-top-15">
				<div class="switch-container">
					<label class="switch"><input type="checkbox"><span class="switch-button"></span><span class="switch-text">Turn on email alerts for this search</span></label>
				</div>

				<div class="sort-by">
					<span>Sort by:</span>
					<select class="selectpicker hide-tick">
						<option>Relevance</option>
						<option>Newest</option>
						<option>Oldest</option>
						<option>Random</option>
					</select>
				</div>
			</div>
 -->
			<div class="listings-container margin-top-35">
				
			<?php while($r = mysqli_fetch_array($c)){  ?>

				<!-- Job Listing -->
				<a href="single-job-page.php?id=<?php echo $r['id'] ?>" class="job-listing">

					<!-- Job Listing Details -->
					<div class="job-listing-details">

						<!-- Logo -->
						<div class="job-listing-company-logo">
							<img src="images/company-logo-01.png" alt="">
						</div>

						<!-- Details -->
						<div class="job-listing-description">
							<h3 class="job-listing-title"><?php echo $r['job_title']; ?></h3>

							<!-- Job Listing Footer -->
							<div class="job-listing-footer">
								<ul>
									<!-- <li><i class="icon-material-outline-business"></i> Hexagon <div class="verified-badge" title="Verified Employer" data-tippy-placement="top"></div></li> -->
									<li><i class="icon-material-outline-location-on"></i><?php echo $r['location']; ?></li>
									<li><i class="icon-material-outline-business-center"></i><?php echo $r['j_time']; ?></li>
									<li><i class="icon-material-outline-access-time"></i> <?php 
										echo  date("Y/m/d");?></li>
								</ul>
							</div>
						</div>

						<!-- Bookmark -->
						<!-- <span class="bookmark-icon"></span> -->
					</div>
				</a>	

			<?php } ?>

			</div>

		</div>
	</div>
</div>


<?php include 'footer.php';?>