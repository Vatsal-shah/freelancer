-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 24, 2019 at 10:44 AM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 7.0.33-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `welancer`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `first_name`, `last_name`) VALUES
(1, 'mandrayusuf@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Yusuf', 'Mandra'),
(2, 'piyush.tank@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Piyush', 'Tank'),
(3, 'frk.mandra@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Faruk', 'Mandra');

-- --------------------------------------------------------

--
-- Table structure for table `bid`
--

CREATE TABLE `bid` (
  `id` int(255) NOT NULL,
  `budget` varchar(255) NOT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date` date NOT NULL,
  `post_id` int(255) NOT NULL,
  `developer_id` int(255) DEFAULT NULL,
  `client_id` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bid`
--

INSERT INTO `bid` (`id`, `budget`, `duration`, `result`, `date`, `post_id`, `developer_id`, `client_id`) VALUES
(8, '2900', NULL, NULL, '2019-05-20', 36, 25, 8),
(9, '3500', NULL, NULL, '2019-02-13', 32, 26, 8),
(10, '3500', NULL, NULL, '2019-02-13', 32, 27, 8),
(11, '3500', NULL, NULL, '2019-02-24', 32, 28, 8),
(12, '3500', NULL, NULL, '2019-02-21', 32, 25, 2);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `email`, `password`, `first_name`, `last_name`) VALUES
(2, 'frk.mandhra@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'faruk', 'mandhra'),
(8, 'yusufmandra846@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'yusuf', 'mandhra');

-- --------------------------------------------------------

--
-- Table structure for table `developer`
--

CREATE TABLE `developer` (
  `id` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `developer`
--

INSERT INTO `developer` (`id`, `email`, `password`, `first_name`, `last_name`) VALUES
(25, 'frk.mandhra@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'faruk', 'mandhra'),
(26, 'test@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Yusuf', 'mandra'),
(27, 'frk.mandhra@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'faruk', 'mandhra'),
(28, 'sarvaiyadhaval167@gamil.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Dhaval', 'Sarvaiya');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(255) NOT NULL,
  `stars` int(5) NOT NULL,
  `reviews` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `type` varchar(255) NOT NULL,
  `post_id` int(255) NOT NULL,
  `client_id` int(255) NOT NULL,
  `developer_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `client_id` int(10) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `j_time` varchar(255) NOT NULL,
  `j_category` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `budget` varchar(255) NOT NULL,
  `j_tags` varchar(255) NOT NULL,
  `j_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `client_id`, `job_title`, `j_time`, `j_category`, `location`, `budget`, `j_tags`, `j_description`) VALUES
(32, 2, 'Laravel Devloper', 'Internship', 'Human Resources', 'India', '50000', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. '),
(33, 8, 'ghjghj', 'Freelance', 'Law Enforcement', 'RAJKOT', '18000', '', 'hjghjhjgh'),
(34, 8, 'fsdf', 'Full Time', 'Accounting and Finance', '', '', '', ''),
(35, 8, 'ttt', 'Freelance', 'Investigative', 'tttxtd', '18000', '', ''),
(36, 8, 'faruk', 'Freelance', 'Miscellaneous', 'RAJKOT', '15000', '', ''),
(44, 8, 'bhavik', 'Internship', 'Miscellaneous', 'RAJKOT', '6000', '', 'qwertyuiop');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` int(255) NOT NULL,
  `date` datetime NOT NULL,
  `bid_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `t_id` int(11) NOT NULL,
  `pro_name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `min` varchar(255) NOT NULL,
  `maximum` varchar(15) NOT NULL,
  `skills` varchar(255) NOT NULL,
  `p_describe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`t_id`, `pro_name`, `category`, `location`, `min`, `maximum`, `skills`, `p_describe`) VALUES
(1, 'laravel', 'Sales & Marketing', 'Rajkot', '1500', '2500', '', 'hllhjuul'),
(2, 'laravel', 'Data Analytics', 'India', '1500', '2500', 'Full Stack Devloper', 'mn'),
(3, 'laravel', 'Legal', 'India', '1500', '2500', 'fghfg', 'fghfhfghfg'),
(4, 'laravel', 'Legal', 'India', '1500', '2500', 'fghfg', 'fghfhfghfg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bid`
--
ALTER TABLE `bid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `developer`
--
ALTER TABLE `developer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`t_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bid`
--
ALTER TABLE `bid`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `developer`
--
ALTER TABLE `developer`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
