<?php session_start();
include 'header.php';


if(!empty($_SESSION['user'])){
	echo "<script>location.href='home.php';</script>";
}

if (isset($_POST['login'])) {

	$email=$_POST['email'];
	$password= md5($_POST['password']);
	$user = $_POST["usertype"];

	
	if ($user == "developer") {	

		$sql = "select * from developer where email= '".$email."' AND password= '".$password."' ";
		$result = $conn->query($sql);

		if ( $result->num_rows > 0 ) {

			$row = $result->fetch_assoc();
			$_SESSION['user'] = $row;
			$_SESSION['user']['type'] = 'developer';				
			echo "<script>location.href='dev_dashboard.php';</script>";

		}else{
			echo "email or password is wrong";
		}
	}

	if ($user == "client") {

		$sql = "select * from client where email= '".$email."' AND password= '".$password."' ";
		$result = $conn->query($sql);

		if ( $result->num_rows > 0 ) {

			$row = $result->fetch_assoc();

			$_SESSION['user'] = $row;
			$_SESSION['user']['type'] = 'client';
			echo "<script>location.href='client_dashboard.php';</script>";
			

		}else{
			echo "email or password is wrong";
		}
	}


}




?>


<!-- Content
================================================== -->
<!-- Category Boxes -->
<div class=" section margin-top-65 margin-bottom-65">
	<div class="container">
		
		<div class="row">
			<div class="col-xl-6 offset-md-3">
				<div class="popup-tabs-container">

			<!-- Login -->
			<div class="popup-tab-content" id="login">
				
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>We're glad to see you again!</h3>
					<span>Don't have an account? <a href="register.php" >Sign Up!</a></span>
				</div>
					
				<!-- Form -->
				<form method="post" action="" id="login-form">

					<div class="account-type">
					<div>
						<input type="radio" name="usertype" id="freelancer-radio" class="account-type-radio" value="developer" checked/>
						<label for="freelancer-radio" class="ripple-effect-dark"><i class="icon-material-outline-account-circle"></i> developer</label>
					</div>

					<div>
						<input type="radio" name="usertype"  id="employer-radio" class="account-type-radio" value="client"/>
						<label for="employer-radio" class="ripple-effect-dark"><i class="icon-material-outline-business-center"></i> client </label>
					</div>
					</div>


					<div class="input-with-icon-left">
						<i class="icon-material-baseline-mail-outline"></i>
						<input type="email" class="input-text with-border" id="emailaddress" placeholder="Email Address" name="email" required/>
					</div>

					<div class="input-with-icon-left">
						<i class="icon-material-outline-lock"></i>
						<input type="password" class="input-text with-border" name="password" id="password" placeholder="Password" required/>
					</div>
					<a href="#" class="forgot-password">Forgot Password?</a>


					<button class="button full-width button-sliding-icon ripple-effect" type="submit" form="login-form" name="login">Log In <i class="icon-material-outline-arrow-right-alt"></i></button>
					<a href="find-work.php" name="find-work"></a>
				</form>
				
				<!-- Button -->
				
				
				<!-- Social Login -->
				<div class="social-login-separator"><span>or</span></div>
				<div class="social-login-buttons">
					<button class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> Log In via Facebook</button>
					<button class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> Log In via Google+</button>
				</div>

			</div>

		</div>
			</div>
		</div>

	</div>
</div>
<!-- Category Boxes / End -->

<?php include 'footer.php'; ?>