<?php session_start();
include 'dev_header.php';	
?>
<?php 

if(empty($_SESSION['user'])){
	echo "<script>location.href='login.php';</script>";
}

?>

<!-- Dashboard Container -->
<div class="dashboard-container">

	<?php include 'dev_sidebar.php'; ?>


	<?php 
	// Hire Developer
	if(isset($_POST['B_id'])){
		
		$accept_query = "update bid set completed_by_developer=1 where id=".$_POST['B_id'];

		if (mysqli_query($conn, $accept_query)) {
		    $status = "Job Completed Successfully by you, Once Client will Complete Job We will Inform You.";
		} else {
		    $status = "Error..";
		}

	}

	?>


	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>My Active Jobs</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Dashboard</a></li>
						<li>My Active Jobs</li>
					</ul>
				</nav>
			</div>
			<?php 

				$b = "SELECT *, B.id as B_id, B.budget as B_budget FROM bid B LEFT JOIN post P ON P.id=B.post_id where B.accept=1 and B.developer_id=".$_SESSION['user']['id'];
				$mt = mysqli_query($conn , $b);

			?>

			<!-- Row -->
			<div class="row">

				<?php 
				if(isset($status)){
				?>
				<div class="col-xl-12">
					<div class="notification success closeable">
					<?php echo $status; ?>
						<a class="close"></a>
					</div>
				</div>	
				<?php	
				}
				?>
				
					
				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">
						
						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-gavel"></i> Jobs List</h3>
						</div>

						<div class="content">
							<ul class="dashboard-box-list">
								<?php while($r = mysqli_fetch_array($mt)){  ?>
								<li>
									<!-- Job Listing -->
									<div class="job-listing width-adjustment">

										<!-- Job Listing Details -->
										<div class="job-listing-details">

											<!-- Details -->
											<div class="job-listing-description">
												<h3 class="job-listing-title"><a href="#"><?php echo $r['job_title']; ?></a></h3>
											</div>
										</div>
									</div>
									
									<!-- Task Details -->
									<ul class="dashboard-task-info">
										<li><strong><?php echo $r['B_budget']; ?></strong><span>Rate</span></li>
										<li><strong><?php echo $r['date']; ?></strong><span>Date</span></li>
									</ul>

									<!-- Buttons -->
									<div class="buttons-to-right always-visible">
										<?php  

										if($r['completed_by_developer'] == '1'){ 

											if( $r['completed_by_client'] == '1'){ ?>
												<span class="dashboard-status-button green">Job Completed By Client.</span>

											<?php }else{ ?>

												<span class="dashboard-status-button green">Pending Approval</span>
											
											<?php } ?>

										<?php }else{ ?>
											<form action="" method="post">
												<input type="hidden" name="B_id" value="<?php echo $r['B_id']; ?>">
												<button type="submit" class="button ripple-effect">Submit Job</button>
											</form>
										<?php }

										?>
									</div>

								</li>
								<?php } ?>
							</ul>	
						</div>
						

					</div>
				</div>

			</div>
			
			<!-- Row / End -->

			<!-- Edit Bid Popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
			<li><a href="#tab">Edit Bid</a></li>
		</ul>

		<div class="popup-tabs-container">

			<!-- Tab -->
			<div class="popup-tab-content" id="tab">
						
					<!-- Bidding -->
					<div class="bidding-widget">
						<!-- Headline -->
						<span class="bidding-detail">Set your <strong>minimal hourly rate</strong></span>

						<!-- Price Slider -->
						<div class="bidding-value">$<span id="biddingVal"></span></div>
						<input class="bidding-slider" type="text" value="" data-slider-handle="custom" data-slider-currency="$" data-slider-min="10" data-slider-max="60" data-slider-value="40" data-slider-step="1" data-slider-tooltip="hide" />
						
						<!-- Headline -->
						<span class="bidding-detail margin-top-30">Set your <strong>delivery time</strong></span>

						<!-- Fields -->
						<div class="bidding-fields">
							<div class="bidding-field">
								<!-- Quantity Buttons -->
								<div class="qtyButtons with-border">
									<div class="qtyDec"></div>
									<input type="text" name="qtyInput" value="2">
									<div class="qtyInc"></div>
								</div>
							</div>
							<div class="bidding-field">
								<select class="selectpicker default with-border">
									<option selected>Days</option>
									<option>Hours</option>
								</select>
							</div>
						</div>
				</div>
				
				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect" type="submit">Save Changes <i class="icon-material-outline-arrow-right-alt"></i></button>

			</div>

		</div>
	</div>
</div>
<!-- Edit Bid Popup / End -->


<?php include 'mini-footer.php'; ?>