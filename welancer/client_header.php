<?php include 'db.php'; ?>
<!doctype html>
<html lang="en">
<head>

<!-- Basic Page Needs
================================================== -->
<title>Hireo</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/colors/blue.css">

</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->
<header id="header-container" class="fullwidth">

	<!-- Header -->
	<div id="header">
		<div class="container">
			
			<!-- Left Side Content -->
			<div class="left-side">
				
				<!-- Logo -->
				<div id="logo">
					<a href="home.php"><img src="images/logo.jpg" alt=""></a>
				</div>

				<!-- Main Navigation -->
				<nav id="navigation">
					<ul id="responsive">

						<li><a href="client_home.php" >Home</a>
							
						</li>

						<!-- <li><a href="browse-job.php">Browse Jobs</a> -->
							<!-- <ul class="dropdown-nav">
								<li>
									
								</li>
								<li><a href="browse-task.php">Browse Tasks</a>
									
								</li>
								
								<li><a href="job-page.php">Job Page</a></li>
								<li><a href="task-page.php">Task Page</a></li>
								<li><a href="company-profile.php">Company Profile</a></li>
							</ul> -->
						</li>

						<li><a href="find-freelancer.php">Find a Freelancer</a>
						<li><a href="job-post.php">Post a Job</a></li>
							<!-- <ul class="dropdown-nav"> -->
								
																	
								<!-- <li><a href="freelancer-profile.php">Freelancer Profile</a></li> -->
								
								<!-- <li><a href="task-post.php">Post a Task</a></li> -->
							<!-- </ul>
						</li> -->

						<li><a href="#">Dashboard</a>
							<ul class="dropdown-nav">
								<li><a href="client_dashboard.php">Dashboard</a></li>
								<!-- <li><a href="messages.php">Messages</a></li> -->
								<li><a href="client_bookmark.php">Bookmarks</a></li>
								<li><a href="client_reviews.php">Reviews</a></li>
								<li><a href="manage-jobs.php">Jobs</a>
									<ul class="dropdown-nav">
										<li><a href="manage-jobs.php">Manage Jobs</a></li>
										<!-- <li><a href="manage-candidates.php">Manage Candidates</a></li> -->
										<li><a href="job-post.php">Post a Job</a></li>
									</ul>
								</li>
								<li><a href="dashboard-manage-tasks.html">Manage</a>
									<ul class="dropdown-nav">
										<!-- <li><a href="manage-tasks.php">Manage Tasks</a></li> -->
										<li><a href="manage-bidders.php">Manage Bidders</a></li>
										<li><a href="client_bids.php">My Active Bids</a></li>
										<!-- <li><a href="task-post.php">Post a Task</a></li> -->
									</ul>
								</li>
								<li><a href="client_settings.php">Settings</a></li>
							</ul>
						</li>

						<li><a href="#">Pages</a>
							<ul class="dropdown-nav">
								<!-- <li><a href="pages-blog.html">Blog</a></li> -->
								<li><a href="client_planes.php">Pricing Plans</a></li>
								<li><a href="client_checkout.php">Checkout Page</a></li>
								<li><a href="client_contact.php">Contact</a></li>
							</ul>
						</li>

					</ul>
				</nav>
				<div class="clearfix"></div>
				<!-- Main Navigation / End -->
				
			</div>
			<!-- Left Side Content / End -->


			<!-- Right Side Content / End -->
			<div class="right-side">

				<!--  User Notifications -->
				<div class="header-widget hide-on-mobile">
					
					<!-- Notifications -->
					<div class="header-notifications">

						<!-- Trigger -->
						<div class="header-notifications-trigger">
							<a href="#"><i class="icon-feather-bell"></i><span>4</span></a>
						</div>

						<!-- Dropdown -->
						<div class="header-notifications-dropdown">

							<div class="header-notifications-headline">
								<h4>Notifications</h4>
								<button class="mark-as-read ripple-effect-dark" title="Mark all as read" data-tippy-placement="left">
									<i class="icon-feather-check-square"></i>
								</button>
							</div>

							<div class="header-notifications-content">
								<div class="header-notifications-scroll" data-simplebar>
									<ul>
										<!-- Notification -->
										<li class="notifications-not-read">
											<a href="dashboard-manage-candidates.html">
												<span class="notification-icon"><i class="icon-material-outline-group"></i></span>
												<span class="notification-text">
													<strong>Michael Shannah</strong> applied for a job <span class="color">Full Stack Software Engineer</span>
												</span>
											</a>
										</li>

										<!-- Notification -->
										<li>
											<a href="dashboard-manage-bidders.html">
												<span class="notification-icon"><i class=" icon-material-outline-gavel"></i></span>
												<span class="notification-text">
													<strong>Gilbert Allanis</strong> placed a bid on your <span class="color">iOS App Development</span> project
												</span>
											</a>
										</li>

										<!-- Notification -->
										<li>
											<a href="dashboard-manage-jobs.html">
												<span class="notification-icon"><i class="icon-material-outline-autorenew"></i></span>
												<span class="notification-text">
													Your job listing <span class="color">Full Stack PHP Developer</span> is expiring.
												</span>
											</a>
										</li>

										<!-- Notification -->
										<li>
											<a href="dashboard-manage-candidates.html">
												<span class="notification-icon"><i class="icon-material-outline-group"></i></span>
												<span class="notification-text">
													<strong>Sindy Forrest</strong> applied for a job <span class="color">Full Stack Software Engineer</span>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>

						</div>

					</div>
					
					<!-- Messages -->
					<div class="header-notifications">
						<div class="header-notifications-trigger">
							<a href="#"><i class="icon-feather-mail"></i><span>3</span></a>
						</div>

						<!-- Dropdown -->
						<div class="header-notifications-dropdown">

							<div class="header-notifications-headline">
								<h4>Messages</h4>
								<button class="mark-as-read ripple-effect-dark" title="Mark all as read" data-tippy-placement="left">
									<i class="icon-feather-check-square"></i>
								</button>
							</div>

							<div class="header-notifications-content">
								<div class="header-notifications-scroll" data-simplebar>
									<ul>
										<!-- Notification -->
										<li class="notifications-not-read">
											<a href="dashboard-messages.html">
												<span class="notification-avatar status-online"><img src="images/user-avatar-small-03.jpg" alt=""></span>
												<div class="notification-text">
													<strong>David Peterson</strong>
													<p class="notification-msg-text">Thanks for reaching out. I'm quite busy right now on many...</p>
													<span class="color">4 hours ago</span>
												</div>
											</a>
										</li>

										<!-- Notification -->
										<li class="notifications-not-read">
											<a href="dashboard-messages.html">
												<span class="notification-avatar status-offline"><img src="images/user-avatar-small-02.jpg" alt=""></span>
												<div class="notification-text">
													<strong>Sindy Forest</strong>
													<p class="notification-msg-text">Hi Tom! Hate to break it to you, but I'm actually on vacation until...</p>
													<span class="color">Yesterday</span>
												</div>
											</a>
										</li>

										<!-- Notification -->
										<li class="notifications-not-read">
											<a href="dashboard-messages.html">
												<span class="notification-avatar status-online"><img src="images/user-avatar-placeholder.png" alt=""></span>
												<div class="notification-text">
													<strong>Marcin Kowalski</strong>
													<p class="notification-msg-text">I received payment. Thanks for cooperation!</p>
													<span class="color">Yesterday</span>
												</div>
											</a>	
										</li>
									</ul>
								</div>
							</div>

							<a href="dashboard-messages.html" class="header-notifications-button ripple-effect button-sliding-icon">View All Messages<i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>

				</div>
				<!--  User Notifications / End -->

								<?php
								$query = "select * from client where id=".$_SESSION['user']['id'];
								$data = mysqli_query($conn, $query);
								$rs = mysqli_fetch_array($data);


								?>

				<!-- User Menu -->
				<div class="header-widget">

					<!-- Messages -->
					<div class="header-notifications user-menu">
						<div class="header-notifications-trigger">
							<a href="#"><div class="user-avatar status-online"><img style="width: 42px; height: 42px;" src="upload/<?php echo $rs['image'];?>" alt=""></div></a>
						</div>

						<!-- Dropdown -->
						<div class="header-notifications-dropdown">

							<!-- User Status -->
							<div class="user-status">
								
								<!-- User Name / Avatar -->
								<div class="user-details">
									<div class="user-avatar status-online"><img src="upload/<?php echo $rs['image'];?>" alt=""></div>
									<div class="user-name">
										<?php echo $_SESSION['user']['first_name']." ".$_SESSION['user']['last_name']; ?> <span><?php  echo ucfirst($_SESSION['user']['type']); ?> </span>
									</div>
								</div>
								
								<!-- User Status Switcher -->
								<!-- <div class="status-switch" id="snackbar-user-status">
									<label class="user-online current-status">Online</label>
									<label class="user-invisible">Invisible</label>
									
									<span class="status-indicator" aria-hidden="true"></span>
								</div>	 -->
						</div>
						
						<ul class="user-menu-small-nav">
							<li><a href="client_dashboard.php"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
							<li><a href="client_settings.php"><i class="icon-material-outline-settings"></i> Settings</a></li>
							<li><a href="logout.php"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
						</ul>

						</div>
					</div>
				</div>
				<!-- User Menu / End -->

				<!-- Mobile Navigation Button -->
				<span class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</span>

			</div>
			<!-- Right Side Content / End -->

		</div>
	</div>
	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->
