<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<head>
<title>Colored  an Admin Panel Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Colored Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link rel="stylesheet" href="css/bootstrap.css">
<!-- //bootstrap-css -->
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<link href='font1.css' rel='stylesheet' type='text/css'>
<!-- font-awesome icons -->
<link rel="stylesheet" href="css/font.css" type="text/css"/>
<link href="css/font-awesome.css" rel="stylesheet">
<link href="\fontawesome-free-5.7.2-web\css" rel='stylesheet' type='text/css' />
<style>
	.ab{
		    position: relative;
			display: table-cell;
			width: 60px;
			text-align: center;
			vertical-align: middle;
			font-size: 18px;
			padding: .7em 0;
	}
</style><!-- //font-awesome icons -->
<script src="js/jquery2.0.3.min.js"></script>
<script src="js/modernizr.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src="js/screenfull.js"></script>
		<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});	
		});
		</script>
<!-- charts -->
<script src="js/raphael-min.js"></script>
<script src="js/morris.js"></script>
<link rel="stylesheet" href="css/morris.css">
<!-- //charts -->
<!--skycons-icons-->
<script src="js/skycons.js"></script>
<!--//skycons-icons-->
</head>


<body class="dashboard-page">
	<script>
	        var theme = $.cookie('protonTheme') || 'default';
	        $('body').removeClass (function (index, css) {
	            return (css.match (/\btheme-\S+/g) || []).join(' ');
	        });
	        if (theme !== 'default') $('body').addClass(theme);
        </script>


	<nav class="main-menu">
		<ul>
			<li>
				<a href="index.php">
					<i class="fa fa-home nav_icon ab"></i>
					<span class="nav-text">
					Dashboard
					</span>
				</a>
			</li>

			<li class="has-subnav">
				<a href="client.php">
					<i class="fa fa-users ab"></i>
						<span class="nav-text">Client</span>
					
				</a>
				
			</li>

			<li class="has-subnav">
				<a href="devlopar.php">
					<i class="fa fa-users ab"></i>
						<span class="nav-text">Devlopar</span>
					
				</a>
				
			</li>

			<li class="has-subnav">
				<a href="category.php">
					<i class="fa fa-th-large ab" ></i>
						<span class="nav-text">category</span>
					
				</a>
				
			</li>

			<li class="has-subnav">
				<a href="subcategory.php">
					<i class="fa fa-th-list ab" aria-hidden="true"></i>
						<span class="nav-text">Subcategory</span>
					
				</a>
				
			</li>
			
			
			<li class="has-subnav">
				<a href="post.php">
					<i class="fa fa-file ab" aria-hidden="true"></i>
						<span class="nav-text">Post</span>
					
				</a>
				
					</li>
			<li class="has-subnav">
				<a href="feedback.php">
				<i class="fa fa-comments ab" aria-hidden="true"></i>
						<span class="nav-text">FeedBack</span>
					
				</a>
				
			</li>



			</li>
			<li class="has-subnav">
				<a href="Secure_code.php">
					<i class="fa fa-unlock ab" aria-hidden="true"></i>
						<span class="nav-text">Change Password</span>
					
				</a>
				
			</li>
			
		<ul class="logout">
			<li>
			<a href="login.php">
			<i class="icon-off nav-icon"></i>
			<span class="nav-text">
			Logout
			</span>

			
			</a>
			</li>
		</ul>
	</nav>
	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
			<i class="icon-proton-logo"></i>
			<i class="icon-reorder"></i>
			</a>
		</nav>
		<section class="title-bar">
			<div class="logo">
				<h1><a href="index.html"><img src="images/logo.png" alt="" />WeLancer</a></h1>
			</div>
			
			
			<div class="clearfix"> </div>
		</section>
		<!-- tables -->
<link rel="stylesheet" type="text/css" href="css/table-style.css" />
<link rel="stylesheet" type="text/css" href="css/basictable.css" />
<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });

      $('#table-swap-axis').basictable({
        swapAxis: true
      });

      $('#table-force-off').basictable({
        forceResponsive: false
      });

      $('#table-no-resize').basictable({
        noResize: true
      });

      $('#table-two-axis').basictable();

      $('#table-max-height').basictable({
        tableWrapper: true
      });
    });
</script>
					
			
			
			