<?php
include "header.php";
?>


		<div class="charts" >		
			<div class="mid-content-top charts-grids" >
				<div class="middle-content" >
						<h1>Feedback Listing</h1>
					
					<!-- start content_slider -->
					<div class="bs-example widget-shadow" data-example-id="hoverable-table"> 
						<div class="panel-body widget-shadow">
						<table class="table table-hover"> 
							<thead> 
								<tr>  
									<th>ID</th> 
									<th>Stars</th> 
									<th>Reviews</th> 
									<th>Date</th> 
									<th>Post_Id</th> 
									<th>Client_Id</th> 
									<th>Devloper_Id</th> 
									<th>Action</th>
								</tr> 
							</thead> 
							<?php
								include "connection.php";
							$c=mysqli_query($con,"select * from feedback");
							while($r=mysqli_fetch_array($c))
							{
							?>
							<tbody> 
								<tr> 
									<td><?php echo $r['id'];?></td> 
									<td><?php echo $r['stars'];?></td> 
									<td><?php echo $r['reviews'];?></td> 
									<td><?php echo $r['date'];?></td> 
									<td><?php echo $r['post_id'];?></td> 
									<td><?php echo $r['client_id'];?></td> 
									<td><?php echo $r['developer_id'];?></td>
									<td>
									
									<a onclick="return confirm('Are you sure you want to delete this Feedback?');" href="fdelete.php?id=<?php echo $r['id'];?>">
									
									<i style ="color:blue;font-size:20px" class="fa fa-trash-o" aria-hidden="true"></i>
									</a>	
									
									</td> 
								</tr> 
							 </tbody>
							<?php
							}
							?>
						</table>
						</div>
					</div>
				</div>
					
			</div>
		</div>
		
				
			</div>
		</div>
	
<?php
include "footer.php";
?>


