-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2020 at 10:27 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `welancer`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `secure_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `first_name`, `last_name`, `secure_code`) VALUES
(1, 'mandrayusuf@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Yusuf', 'Mandra', ''),
(2, 'piyush.tank@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Piyush', 'Tank', ''),
(3, 'frk.mandra@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Faruk', 'Mandra', ''),
(4, 'sarvaiyadhaval167@gmail.com', 'dhaval', 'Dhaval', 'Sarvaiya', 'dr');

-- --------------------------------------------------------

--
-- Table structure for table `bid`
--

CREATE TABLE `bid` (
  `id` int(255) NOT NULL,
  `budget` varchar(255) NOT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `accept` varchar(255) DEFAULT NULL,
  `date` date NOT NULL,
  `post_id` int(255) NOT NULL,
  `developer_id` int(255) DEFAULT NULL,
  `client_id` int(255) DEFAULT NULL,
  `completed_by_developer` varchar(255) DEFAULT NULL,
  `completed_by_client` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bid`
--

INSERT INTO `bid` (`id`, `budget`, `duration`, `accept`, `date`, `post_id`, `developer_id`, `client_id`, `completed_by_developer`, `completed_by_client`) VALUES
(10, '3500', NULL, '0', '2019-02-13', 32, 27, 2, NULL, NULL),
(11, '3500', NULL, '0', '2019-02-24', 32, 28, 2, NULL, NULL),
(17, '3500', NULL, '0', '2019-03-17', 32, 26, 2, NULL, NULL),
(18, '3500', NULL, '1', '2019-03-22', 33, 26, 2, NULL, NULL),
(19, '3600', NULL, '1', '2019-03-30', 48, 25, 2, '1', '1'),
(20, '3950', NULL, '0', '2019-03-21', 45, 25, 8, NULL, NULL),
(22, '4500', NULL, '0', '2019-04-25', 34, 25, 8, NULL, NULL),
(23, '2900', NULL, '1', '2019-04-20', 45, 25, 8, '1', '1'),
(24, '3100', NULL, '0', '2019-04-19', 34, 25, 8, '1', '1'),
(25, '3300', NULL, '1', '2019-04-16', 34, 25, 8, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `image`) VALUES
(1, 'CSS', 'download.png'),
(2, 'HTML5', 'html.png'),
(4, 'css5', 'p18.png'),
(5, 'python', 'Screenshot (13).png');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `email`, `password`, `first_name`, `last_name`, `image`) VALUES
(2, 'frk.mandhra@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'faruk', 'mandhra', 'p18.png'),
(8, 'yusufmandra846@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'yusuf', 'mandhra', 'Screenshot (10).png'),
(9, 'sarvaiyadhaval167@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Dhaval', 'Sarvaiya', 'IMG_9549.jpg'),
(10, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `developer`
--

CREATE TABLE `developer` (
  `id` int(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `experiance` varchar(50) NOT NULL,
  `contect_no` int(10) NOT NULL,
  `skill` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `developer`
--

INSERT INTO `developer` (`id`, `email`, `password`, `first_name`, `last_name`, `gender`, `experiance`, `contect_no`, `skill`, `image`) VALUES
(25, 'frk.mandhra@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'faruk', 'mandhra', '', '', 0, '', 'UNADJUSTEDNONRAW_thumb_1a0.jpg'),
(26, 'mandrayusuf@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Yusuf', 'mandra', '', '', 0, '', ''),
(27, 'frk.mandhra@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'faruk', 'mandhra', '', '', 0, '', ''),
(28, 'sarvaiyadhaval167@gamil.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Dhaval', 'Sarvaiya', '', '', 0, '', ''),
(29, 'sarvaiyadhaval7@yahoo.com', '81dc9bdb52d04dc20036dbd8313ed055', 'DHAVAL', 'SARVAIYA', '', '', 0, '', 'Wallpaper (34).jpg');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(255) NOT NULL,
  `stars` int(5) NOT NULL,
  `reviews` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `post_id` int(255) NOT NULL,
  `client_id` int(255) NOT NULL,
  `developer_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `stars`, `reviews`, `date`, `post_id`, `client_id`, `developer_id`) VALUES
(1, 5, 'fgdfhdg', '2019-04-17 00:00:00', 2, 6, 45);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `client_id` int(10) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `j_time` varchar(255) NOT NULL,
  `j_category` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `budget` varchar(255) NOT NULL,
  `j_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `client_id`, `job_title`, `j_time`, `j_category`, `location`, `budget`, `j_description`) VALUES
(34, 8, 'Php Developer', 'Full Time', 'Accounting and Finance', '', '', ''),
(44, 8, 'Node Js Devloper', 'Internship', 'Clerical & Data Entry', 'RAJKOT', '6000', 'qwertyuiop'),
(45, 8, 'java project', '', 'IT and Computers', 'rajkot', '12000', 'project for java'),
(46, 2, 'php developers', '', 'IT and Computers', 'india', '30000', 'php'),
(47, 2, 'welancers', '', 'IT and Computers', 'gujrat', '35000', 'project'),
(48, 2, 'click to service', 'full time', 'Accounting and Finance', 'kothariya', '10000', 'project'),
(50, 1, 'News', 'full time', 'Clerical & Data Entry', 'Rajkot', '5000', 'Designer'),
(51, 1, 'News2', 'full time', 'Clerical & Data Entry', 'Rajkot4', '5000', 'Designer'),
(52, 10, 'sport', 'full time', 'Accounting and Finance', 'Rajkot1', '5', 'Designer');

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` int(255) NOT NULL,
  `date` datetime NOT NULL,
  `bid_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` int(255) NOT NULL,
  `image` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `name`, `category_id`, `image`) VALUES
(1, 'html', 1, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bid`
--
ALTER TABLE `bid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `developer`
--
ALTER TABLE `developer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bid`
--
ALTER TABLE `bid`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `developer`
--
ALTER TABLE `developer`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
